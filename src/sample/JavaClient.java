package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import org.apache.xmlrpc.XmlRpcClient;
import org.apache.xmlrpc.XmlRpcException;

import java.io.IOException;
import java.util.Vector;

public class JavaClient extends Application {
    public Label txtHeading;
    public TextField edtIpAddress;
    public TextField edtPortNumber;
    public Button btnConnect;
    public ImageView imgRock = new ImageView();
    public ImageView imgPaper;
    public ImageView imgScissors;
    public Button btnRock;
    public Button btnPaper;
    public Button btnScissors;
    public Label txtResult;
    public int userChoice;
    public Button btnReset;
    public Label txHelp;
    private Vector<Object> params;
    private XmlRpcClient server;
    private String ipAddress;
    private String port;

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));

        primaryStage.getIcons().add(new Image("/sample/icon.png"));
        primaryStage.setTitle("Rock! Paper! Scissors!");
        Scene scene = new Scene(root, 400, 250);
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        scene.getStylesheets().add(JavaClient.class.getResource("style.css").toExternalForm());
        primaryStage.show();
    }

    public void resetScore(){
        try {
            server = new XmlRpcClient("http://"+ipAddress+":"+port+"/RPC2");
            params = new Vector<>();
            Object resetResponseFromServer = server.execute("sample.resetScore", params);
            String resetResponse = (String) resetResponseFromServer;
            txtResult.setText("Score reset "+resetResponse);
            if(resetResponse.equals("successfully")){
                btnReset.setDisable(false);
            }
        } catch (XmlRpcException | IOException e) {
            e.printStackTrace();
            txtResult.setText("Score reset failed");
        }
    }

    public void connect(){
        ipAddress = edtIpAddress.getText();
        port = edtPortNumber.getText();
        if(ipAddress.isEmpty() || port.isEmpty()) {
            txHelp.setText("Please enter port number and ip address to connect");
        } else{
            txHelp.setText("Choose either rock, paper or scissors");
            try {
                server = new XmlRpcClient("http://"+ipAddress+":"+port+"/RPC2");
                params = new Vector<>();
                Object connResponseFromServer = server.execute("sample.connection", params);
                String connectionResponse = (String) connResponseFromServer;
                txtResult.setText("Connection "+connectionResponse);
                if(connectionResponse.equals("successful")){
                    btnConnect.setText("Connected");
                    btnConnect.setDisable(true);
                    edtIpAddress.setDisable(true);
                    edtPortNumber.setDisable(true);
                    btnRock.setDisable(false);
                    btnPaper.setDisable(false);
                    btnScissors.setDisable(false);
                }
            } catch (XmlRpcException | IOException e) {
                e.printStackTrace();
                txtResult.setText("Connection Failed");
                btnConnect.setDisable(false);
                edtIpAddress.setDisable(false);
                edtPortNumber.setDisable(false);
            }
        }
    }

    public void sendRequest(int userChoice){
        connect();
        try{
            params.addElement(userChoice);
            Object resultFromServer = server.execute("sample.RockPaperScissors", params);
            String resultToDisplay = (String) resultFromServer;
            txtResult.setText(resultToDisplay);
            btnReset.setDisable(false);
        }catch(Exception exception){
            System.err.println("JavaClient exception : " + exception);
            txtResult.setText("Connection Failed");
        }
    }

    public void btnRockClick(){ sendRequest(1); }
    public void btnPaperClick(){ sendRequest(2); }
    public void btnScissorsClick(){ sendRequest(3); }

    public static void main(String[] args) {
        launch(args);
    }
}